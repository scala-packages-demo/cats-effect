scalaVersion := "3.2.1"

libraryDependencies += "org.typelevel" %% "cats-effect" % "3.4.4"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds",
  "-Xfatal-warnings")
