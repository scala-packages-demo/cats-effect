package example

import cats.effect.{ExitCode, IO, IOApp}

object Arguments extends IOApp {
  def run(args: List[String]): IO[ExitCode] =
    if (args.headOption.map(_ == "--do-it").getOrElse(false))
      IO.println("I did it!").as(ExitCode.Success)
    else
      IO.println("Didn't do it").as(ExitCode(-1))
}

// https://index.scala-lang.org/typelevel/cats-effect
