package example

import cats.effect.{IO, IOApp}

object Hello extends IOApp.Simple {
  val run = IO.println("Hello, World!")
}

// https://index.scala-lang.org/typelevel/cats-effect
