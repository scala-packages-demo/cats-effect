package example

import cats.effect.{IO, IOApp}

object For extends IOApp.Simple {
  val run = for {
    _ <- IO.println("Hello")
    _ <- IO.println("World")
  } yield ()
}

// https://typelevel.org/cats-effect/docs/concepts
